PREFIX = /usr/local

CFLAGS = -std=c99 -Wall -Wextra -pedantic -O0 -ggdb

BIN = rpt
SRC = rpt.c
OBJ = rpt.o

MAN_SRC = rpt.1
MAN_PAGE = rpt.1.gz

.PHONY: all compile man clean install uninstall

all: compile man

compile: $(BIN)

$(BIN): $(OBJ)
	cc $^ -o $@

%.o: %.c
	cc $(CFLAGS) -c $< -o $@

man: $(MAN_PAGE)

%.1.gz: %.1
	gzip -9 -c $< > $@

clean:
	rm -f $(BIN) $(OBJ) $(MAN_PAGE)

install: compile man
	cp -f $(BIN) $(PREFIX)/bin
	cp -f $(MAN_PAGE) $(PREFIX)/share/man/man1

uninstall:
	rm -f $(PREFIX)/bin/$(BIN) $(PREFIX)/share/man/man1/$(MAN_PAGE)

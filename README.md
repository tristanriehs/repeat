rpt - Easily repeat a shell command
===

Repeat provides a simple way to repeat *COUNT* times a command *CMD*.

```shell
rpt -n COUNT CMD
```

My claim is that it is simpler to write than the following :

```shell
i=0
while [ $i -lt COUNT]; do
		CMD
		i=$((i+1))
done
```

Especially from the command line. The use of `rpt` also makes the execution
slightly faster, but performance is not the main goal.

# Installation

## Dependencies

None, except the C standard library.

## Build dependencies

- C compiler (`rpt` executable)
- `cp` (installation)
- `gzip` (manual page)
- GNU Make (build system)

## Installation

To compile, run :

```shell
make
```

To install, run :

```shell
make install
```
